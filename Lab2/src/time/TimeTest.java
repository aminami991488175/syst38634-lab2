package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {
	
	
//	@Test
//	public void testgetTotalMillisecondsRegular() {
//		fail("Invalid number of milliseconds");
//	}
	
	@Test
	public void testgetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testgetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test 
	public void testgetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testgetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		fail("Invalid number of milliseconds");
	}
	
	
	
	
	
	
	
	
	
	

	//@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	//@Test (expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0a");
		assertFalse("The time provided is not valid", totalSeconds == 3661);
	}
	//@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		assertTrue("The time provided does not match the result", totalSeconds == 3719);
	}
	
	//@Test (expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertFalse("The time provided does not match the result", totalSeconds == 3720);
	}
	 

	
	

}
